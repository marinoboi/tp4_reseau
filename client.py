import socket, getpass
from socketUtils import send_msg, recv_msg

ENTETE_RECEPTION_INFOS_CONNECTION = "INFOSCONNECTION"
ENTETE_RECEPTION_CHOIX_COURRIEL = "CHOIXCOURRIEL"

while True:
    success = False
    # Menu de connexion
    while success is False:
        firstVar = input("Menu de connexion\n1. Créer un compte\n2. Se connecter\nVotre choix : ")
        if firstVar == "1" or firstVar == "2":
            username = input("Nom d'usager : ") 
            password = getpass.getpass(prompt="Mot de passe : ")
            serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            serverSocket.connect(("localhost", 1234))
            send_msg(serverSocket, ENTETE_RECEPTION_INFOS_CONNECTION + "," + username + "," + password + "," + firstVar)
            response1 = recv_msg(serverSocket)
            usrValidation = response1.split("+")[0]
            pwdValidation = response1.split("+")[1]
            print(usrValidation + "\n" + pwdValidation)
            # Scénario de création de compte
            if (firstVar == "1"):
                if (pwdValidation == "Votre mot de passe est valide.") and (usrValidation == "Votre nom d'utilisateur est valide."):
                    success = True
            # Scénario de connexion
            elif (firstVar == "2"):
                if (usrValidation == "Utilisateur " + username + " connecté avec succès."):
                    success = True
        else:
            print("Veuillez sélectionner une des options proposées.\n")
    # Menu principal
    secondVar = None
    # Quitter seulement le menu principal lorsque l'utilisateur choisit l'option 4
    while secondVar != "4":
        secondVar = input("Menu principal\n1. Consultation de courriels\n2. Envoi de courriels\n3. Statistiques\n4. Quitter\nVotre choix : ")
        send_msg(serverSocket, ENTETE_RECEPTION_CHOIX_COURRIEL + "," + secondVar)
        # Scénario consultation de courriels
        if secondVar == "1":
            # Ne pas sortir de ce menu tant qu'un choix valide aura été fait si l'utilisateur possède des courriels
            success = False
            while success is False:
                print("\nConsultation de courriels\n")
                emailListResponse = recv_msg(serverSocket)
                print(emailListResponse)
                if emailListResponse != "Vous n'avez aucun courriel.":
                    send_msg(serverSocket, input("Entrer le nombre correspondant au courriel choisi : "))
                    viewEmailResponse = recv_msg(serverSocket)
                    print(viewEmailResponse)
                    if viewEmailResponse != "Veuillez sélectionner une des options proposées.":
                        success = True
                else:
                    success = True
                input("Appuyez sur Enter pour continuer : ")
                print("")
        # Scénario envoi de courriel
        elif secondVar == "2":
            print("\nEnvoi de courriel\n")
            recipient = input("Destinataire : ")
            title = input("Objet : ")
            message = input("Corps : \n")
            send_msg(serverSocket, recipient + "," + title + "," + message)
            emailSentResponse = recv_msg(serverSocket)
            print(emailSentResponse)
            print("")
        # Scénario statistiques
        elif secondVar == "3":
            print("\nStatistiques\n")
            statsResponse = recv_msg(serverSocket)
            print("Nombre de messages : " + statsResponse.split(",")[0] + "\nTaille totale du dossier : " + statsResponse.split(",")[1] + " bytes\nListe des messages : \n" + statsResponse.split(",")[2])
            input("Appuyez sur Enter pour continuer : ")
            print("")
        # Scénario quitter le programme
        elif secondVar == "4":
            print("\nÀ la prochaine!\n")
            serverSocket.close()
