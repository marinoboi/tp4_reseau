import socket, select, argparse, sys, os, json, smtplib, re
from socketUtils import send_msg, recv_msg
from email.mime.text import MIMEText
from os.path import getsize
from hashlib import sha512

def getPwdValidationMessage(pwd):
    if not len(pwd) >= 8 or not len(pwd) <= 20:
        return "Votre mot de passe doit contenir entre 8 et 20 caractères."
    if not re.search(r"[0-9]{1,}", pwd):
        return "Votre mot de passe doit contenir au moins un chiffre."
    if not re.search(r"[A-Z]{2,}", pwd):
        return "Votre mot de passe doit contenir au moins deux lettres majuscules."
    if not re.search(r"[a-z]{2,}", pwd):
        return "Votre mot de passe doit contenir au moins deux lettres minuscules."
    return "Votre mot de passe est valide."
    
def getInternalUsrValidationMessage(username):
    if not re.search(r"(@ift.glo2000.ca)$", username):
        return "Votre nom d'utilisateur doit finir par @ift.glo2000.ca"
    return "Votre nom d'utilisateur est valide."

def getExternalUsrValidationMessage(recipient):
    if not re.search(r"^[^ @]+@[^@ ]+\.[^ @]+$", recipient):
        return "Cette adresse courriel est invalide."
    return "Cette adresse courriel est valide."


socServeur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socServeur.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
socServeur.bind(("localhost", 1234))
socServeur.listen(5)

listeClients = []

while True:
    success = False
    while success is False:
        print("Écoute du port " + str(socServeur.getsockname()[1]))
        (clientSocket, addr) = socServeur.accept()
        userInfo = recv_msg(clientSocket)
        # Vérifier que le message contienne la bonne entête
        if userInfo.split(",")[0] == "INFOSCONNECTION":
            username = userInfo.split(",")[1]
            pwd = userInfo.split(",")[2]
            firstVar = userInfo.split(",")[3]
            # Accéder au dossier de l'utilisateur
            userDir = os.path.dirname(os.path.abspath(__file__)) + "/" + username
            # Scénario de création de compte
            if firstVar == "1":
                # Vérifier que l'utilisateur n'existe pas
                if not os.path.exists(userDir):
                    # Vérifier que le mot de passe et le username entrés sont valides
                    usrValidationMessage = getInternalUsrValidationMessage(username)
                    pwdValidationMessage = getPwdValidationMessage(pwd)                
                    if pwdValidationMessage == "Votre mot de passe est valide." and usrValidationMessage == "Votre nom d'utilisateur est valide.":
                        os.mkdir(userDir)
                        pwdTxtFile = open(userDir + "/password.txt", "w+")
                        pwdTxtFile.write(sha512(pwd.encode()).hexdigest())
                        pwdTxtFile.close()
                    msg = usrValidationMessage + "+" + pwdValidationMessage
                    success = True
                else:
                    msg = "Cet utilisateur existe déjà. Veuillez réessayer." + "+"
                send_msg(clientSocket, msg)
            # Scénario de connexion
            elif firstVar == "2":
                # Vérifier que l'utilisateur existe
                if os.path.exists(userDir):
                    hashedPwd = sha512(pwd.encode()).hexdigest()
                    pwdTxtFile = open(userDir + "/password.txt", "r")
                    actualHashedPwd = pwdTxtFile.read()
                    pwdTxtFile.close()
                    # Vérifier que le mot de passe entré est le bon
                    if hashedPwd == actualHashedPwd:
                        msg = "Utilisateur " + username + " connecté avec succès." + "+"
                        success = True
                    else:
                        msg = "Le mot de passe est incorrect." + "+"
                else:
                    msg = "Utilisateur " + username + " non existant." + "+"
                send_msg(clientSocket, msg)
    secondVar = None
    # Quitter seulement le menu principal lorsque l'utilisateur choisit l'option 4
    while secondVar != "4":
        print("Attente d'un choix...")
        secondMenuInfo = recv_msg(clientSocket)
        if secondMenuInfo.split(",")[0] == "CHOIXCOURRIEL":      
            secondVar = secondMenuInfo.split(",")[1]
            # Scénario consultation de courriels
            if secondVar == "1":
                success = False
                while success is False:
                    userDir = os.path.dirname(os.path.abspath(__file__)) + "/" + username
                    fileList = os.listdir(userDir)
                    counter = 0
                    emailList = []
                    msg = ""                
                    for obj in fileList:
                        if obj != "password.txt":
                            msg += str(counter) + ". " + obj.replace(".txt","") + "\n"
                            emailList.append(obj)
                            counter += 1
                    if len(emailList) == 0:
                        msg = "Vous n'avez aucun courriel."
                        success = True
                    send_msg(clientSocket, msg)
                    if len(emailList) >= 1:                    
                        emailIndex = recv_msg(clientSocket)
                        msg2 = ""
                        if int(emailIndex) + 1 <= len(emailList):
                            chosenEmail = emailList[int(emailIndex)]
                            emailTxtFile = open(userDir + "/" + chosenEmail, "r")
                            msg2 = emailTxtFile.read()
                            emailTxtFile.close()
                            success = True
                        else:
                            msg2 = "Veuillez sélectionner une des options proposées."
                        send_msg(clientSocket, msg2)
            # Scénario envoi de courriel
            elif secondVar == "2":
                newEmailMsg = recv_msg(clientSocket)
                recipient = newEmailMsg.split(",")[0]
                title = newEmailMsg.split(",")[1]
                message = newEmailMsg.split(",")[2]
                if getInternalUsrValidationMessage(recipient) == "Votre nom d'utilisateur est valide.":
                    recipientUserDir = os.path.dirname(os.path.abspath(__file__)) + "/" + recipient
                    msg = ""
                    if os.path.exists(recipientUserDir):
                        newEmailTxtFile = open(recipientUserDir + "/" + title + ".txt", "w+")
                        msg = "Le courriel a bien été envoyé!"
                    else:
                        errorDir = os.path.dirname(os.path.abspath(__file__)) + "/ERREUR"
                        msg = "L'envoi n'a pas pu être effectué."
                        if not os.path.isdir(errorDir):
                            os.mkdir(errorDir)
                        newEmailTxtFile = open(errorDir + "/" + title + ".txt", "w+")
                    newEmailTxtFile.write(message)
                    newEmailTxtFile.close()
                    send_msg(clientSocket, msg)
                elif getExternalUsrValidationMessage(recipient) == "Cette adresse courriel est valide.":
                    courriel = MIMEText(message)
                    courriel["From"] = username
                    courriel["To"] = recipient
                    courriel["Subject"] = title                    
                    try :
                        smtpConnection = smtplib.SMTP(host="smtp.ulaval.ca", timeout=10)
                        smtpConnection.sendmail(username, recipient, courriel.as_string())
                        smtpConnection.quit()
                        msg = "Le courriel a bien été envoyé!"
                    except :
                        msg = "L'envoi n'a pas pu être effectué."
                    send_msg(clientSocket, msg)
            # Scénario statistiques
            elif secondVar == "3":
                userDir = os.path.dirname(os.path.abspath(__file__)) + "/" + username
                emailList = os.listdir(userDir)
                counter = 0
                folderSize = 0
                listEmail = ""
                for obj in emailList:
                    if obj != "password.txt":
                        listEmail += obj.replace(".txt","") + "\n"
                        counter += 1
                        folderSize += getsize(userDir +  "/" + obj)
                send_msg(clientSocket, str(counter) + "," + str(folderSize) + "," + listEmail)
            # Scénario quitter le programme
            elif secondVar == "4":
                clientSocket.close()
